const getters = {
    token: state => state.user.token,
    userId: state => state.user.id,
    username: state => state.user.name,
    phone: state => state.user.phone,
    nettyAddr: state => state.user.nettyAddr,
}
export default getters
