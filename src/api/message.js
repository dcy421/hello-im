import axios from '_l/api.request'

/**
 * 获取离线消息（Group）
 * @param groupId
 * @returns {*}
 */
export const getGroupOffMsg = (groupId) => {
    return axios.request({
        url: '/off-group-mes/getGroupOffMsg',
        params: {
            groupId: groupId
        }
    })
}

/**
 * 获取离线消息（P2P）
 * @param userId
 * @param receiveUserId
 * @returns {*}
 */
export const getP2pOffMsg = (userId, receiveUserId) => {
    return axios.request({
        url: '/off-p2p-mes/getP2pOffMsg',
        params: {
            userId: userId,
            receiveUserId: receiveUserId
        }
    })
}