import axios from '_l/api.request'

/**
 * 检查用户是否在黑名单中
 * @param data
 * @returns {AxiosPromise}
 */
export const isBlackList = (fromUserId,toUserId) => {
    return axios.request({
        url: '/black-list/isBlackList',
        method: 'get',
        params: {
            fromUserId: fromUserId,
            toUserId: toUserId
        }
    })
}

/**
 * 添加黑名单
 * @param fromUserId  toUserId
 * @returns {AxiosPromise}
 */
export const addBlackList = (fromUserId,toUserId) => {
    return axios.request({
        url: '/black-list/addBlackList',
        method: 'post',
        params: {
            fromUserId: fromUserId,
            toUserId: toUserId
        }
    })
}

/**
 * 删除黑名单
 * @param userId
 * @returns {AxiosPromise}
 */
export const deleteBlackList = (fromUserId,toUserId) => {
    return axios.request({
        url: '/black-list/deleteBlackList',
        method: 'post',
        params: {
            fromUserId: fromUserId,
            toUserId: toUserId
        }
    })
}

/**
 * 获取用户黑名单列表
 * @param userId
 * @returns {AxiosPromise}
 */
export const getBlackListByUserId = (userId) => {
    return axios.request({
        url: '/black-list/getBlackListByUserId',
        method: 'get',
        params: {
            userId: userId
        }
    })
}