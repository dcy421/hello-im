import axios from '_l/api.request'

/**
 * 获取群组的用户
 * @param data
 * @returns {AxiosPromise}
 */
export const getUserListByGroupId = (groupId) => {
    return axios.request({
        url: '/group-user/getUserListByGroupId',
        method: 'get',
        params: {
            groupId: groupId
        }
    })
}

/**
 * 检查是否为群组成员
 * @param groupId  userId
 * @returns {AxiosPromise}
 */
export const addBlackList = (groupId,userId) => {
    return axios.request({
        url: '/group-user/isOrNotGroupUser',
        method: 'get',
        params: {
            groupId: groupId,
            userId: userId
        }
    })
}

/**
 * 在群组中禁言某个用户、解除禁言
 * @param userId
 * @returns {AxiosPromise}
 */
export const updateTalkStatus = (data) => {
    return axios.request({
        url: '/group-user/updateTalkStatus',
        method: 'post',
        data:data
    })
}

/**
 * 检查是否被禁言
 * @param groupId  userId
 * @returns {AxiosPromise}
 */
export const isOrNotTalk = (groupId,userId) => {
    return axios.request({
        url: '/group-user/isOrNotTalk',
        method: 'get',
        params: {
            groupId: groupId,
            userId: userId
        }
    })
}

/**
 * 删除群组所有成员
 * @param groupId  userId
 * @returns {AxiosPromise}
 */
export const deleteAllGroupUser = (groupId) => {
    return axios.request({
        url: '/group-user/deleteAllGroupUser',
        method: 'post',
        params: {
            groupId: groupId
        }
    })
}

/**
 * 删除群组成员
 * @param groupId  userId
 * @returns {AxiosPromise}
 */
export const deleteGroupUser = (groupId,userId) => {
    return axios.request({
        url: '/group-user/deleteGroupUser',
        method: 'post',
        params: {
            groupId: groupId,
            userId: userId
        }
    })
}

/**
 * 检查是否为群主
 * @param groupId  userId
 * @returns {AxiosPromise}
 */
export const isOrNotGroupManager = (groupId,userId) => {
    return axios.request({
        url: '/group-user/isOrNotGroupManager',
        method: 'get',
        params: {
            groupId: groupId,
            userId: userId
        }
    })
}