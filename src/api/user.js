import axios from '_l/api.request'

/**
 * 登录
 * @param data
 * @returns {AxiosPromise}
 */
export const login = (data) => {
    return axios.request({
        url: '/login',
        method: 'post',
        data: {
            username: data.username.trim(),
            password: data.password.trim(),
        }
    })
}

/**
 * 获取用户信息
 * @returns {AxiosPromise}
 */
export const getInfo = () => {
    return axios.request({
        url: '/getUserInfo',
        method: 'get'
    })
}

/**
 * 获取netty server addr
 * @returns {*}
 */
export const getNettyServerAddr = () => {
    return axios.request({
        url: '/getNettyServerAddr',
        method: 'get'
    })
}

/**
 * 获取好友列表
 * @param userId
 * @returns {AxiosPromise}
 */
export const getFriendsByUserId = (userId) => {
    return axios.request({
        url: '/user/getFriendsByUserId',
        method: 'get',
        params: {
            userId: userId
        }
    })
}

/**
 * 群组列表
 * @param userId
 * @returns {AxiosPromise}
 */
export const getGroupByUserId = (userId) => {
    return axios.request({
        url: '/group/getGroupByUserId',
        method: 'get',
        params: {
            userId: userId
        }
    })
}

/**
 * 获取群组的用户
 * @param userId
 * @returns {AxiosPromise}
 */
export const getUserByGroupId = (groupId) => {
    return axios.request({
        url: '/group/getUserByGroupId',
        method: 'get',
        params: {
            groupId: groupId
        }
    })
}

/**
 * 修改个人信息
 * @param user
 * @return
 */
export const updateByIdPersonalInformation = (data) => {
    return axios.request({
        url: '/user/updateUser',
        method: 'post',
        data:data
    })
}

/**
 * 获取用户个人信息
 * @returns
 */
export const getUser = (userId) => {
    return axios.request({
        url: '/user/selectUser',
        method: 'get',
        params:{
            id:userId
        }
    })
}

/**
 * 退出
 * @param data
 * @returns {AxiosPromise}
 */
export const logout = (data) => {
    return axios.request({
        url: '/logout',
        method: 'get',
        params:{
            userId:data
        }
    })
}